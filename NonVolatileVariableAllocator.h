#pragma once

#include <stdint.h>
#include "DataManager.h"

#pragma pack(push,1)
struct framData_t
{
	uint32_t fram_data1;
	uint32_t fram_data2;
	uint32_t fram_data3;
	uint32_t fram_data4;
	uint32_t fram_data5;
	uint32_t fram_data6;
};
#pragma pack(pop)

#define OFFSET(x)  (offsetof(framData_t,x))

extern DataManager dataManager;

/*
 * USAGE
 *
 * 	Variable <uint32_t> myData(OFFSET(fram_data1), 120);
 *	uint32_t newData = myData.value();
 *	myData = 44;
 */


