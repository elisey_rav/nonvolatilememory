#pragma once

#include "NonVolatileVariableAllocator.h"

template <typename T>
class Variable {
public:
	Variable(size_t _dataStructureOffset, T _defaultValue = 0)
		:	dataStructureOffset(_dataStructureOffset),
		 	defaultValue(_defaultValue)
	{
		int result = readFromMemrory();
		if (result != 0)	{
			data = defaultValue;
			//TODO
		}
	}

	T value()
	{
		return data;
	}

	void setData(T newData)
	{
		if (newData != data)	{
			data = newData;
			writeToMemory();
		}
	}

	const T& operator=(T const& x)
	{
		setData( x );
		return data;
	}

private:
	int writeToMemory()
	{
		return dataManager.saveDataBlock(&data, sizeof(T), dataStructureOffset);
	}

	int  readFromMemrory()
	{
		return dataManager.loadDataBlock(&data, sizeof(T), dataStructureOffset);
	}

	size_t dataStructureOffset;
	T data;
	T defaultValue;
};
