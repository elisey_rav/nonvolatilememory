#include "fm25clxx.h"

#include "fm25clxx_HAL.h"

#include <string.h>

#include <assert.h>

#define debug_printf(X, p...)		((void)0)

static const uint8_t opcodeWREN =  0b00000110;		//Set write enable latch
static const uint8_t opcodeWRDI =  0b00000100;		//Write disable
static const uint8_t opcodeRDSR =  0b00000101;		//Read Status Register
static const uint8_t opcodeWRSR =  0b00000001;		//Write Status Register
static const uint8_t opcodeREAD =  0b00000011;		//Read memory data
static const uint8_t opcodeWRITE = 0b00000010;		//Write memory data

#define MAX_WRITE_BLOCK_SIZE_DEF	4
static const uint8_t MAX_WRITE_BLOCK_SIZE = MAX_WRITE_BLOCK_SIZE_DEF;
static const uint8_t MAX_WRITE_TRYES = 5;

Fm25clxx::Fm25clxx(const unsigned int _size)
	:	MEMORY_SIZE(_size)
{
	Fm25clxx_HAL_Init();
}

int Fm25clxx::writeBlock( const void* buf, size_t blockSize, uint16_t blockAdr , bool check)
{
	assert( blockAdr + blockSize <= MEMORY_SIZE );

	if (check == false)	{
		write(buf, blockSize, blockAdr);
		return 0;
	}

	assert(blockSize <= MAX_WRITE_BLOCK_SIZE);
	uint8_t readBuf[MAX_WRITE_BLOCK_SIZE];
	int i;
	for (i = 0; i < MAX_WRITE_TRYES; ++i) {
		write(buf, blockSize, blockAdr);
		readBlock((void*)readBuf, blockSize, blockAdr);
		if (memcmp(buf, (void*)readBuf, blockSize) == 0)	{
			//Запись успешна
			return 0;
		}
		debug_printf("FRAM: write mismatch\n");
	}
	debug_printf("FRAM: write fail\n");
	return (-1);
}

void Fm25clxx::readBlock(void* buf, size_t blockSize, uint16_t blockAdr)
{
	assert( blockAdr + blockSize <= MEMORY_SIZE );

	Fm25clxx_HAL_ChipSelect();
	Fm25clxx_HAL_SpiTransfer(opcodeREAD);
	Fm25clxx_HAL_SpiTransfer( (blockAdr >> 8) & 0xff );
	Fm25clxx_HAL_SpiTransfer( blockAdr & 0xff );

	size_t i;
	uint8_t *ptrData = (uint8_t*)buf;
	for (i = 0; i < blockSize; ++i) {
		ptrData[i] = Fm25clxx_HAL_SpiTransfer(0);
	}
	Fm25clxx_HAL_ChipDeselect();
}

void Fm25clxx::writeStatus(uint8_t newSatus)
{
	sendWriteEnableOpcode();

	Fm25clxx_HAL_ChipSelect();
	Fm25clxx_HAL_SpiTransfer(opcodeWRSR);
	Fm25clxx_HAL_SpiTransfer(newSatus);
	Fm25clxx_HAL_ChipDeselect();
}

uint8_t Fm25clxx::readStatus()
{
	Fm25clxx_HAL_ChipSelect();

	Fm25clxx_HAL_SpiTransfer(opcodeRDSR);
	uint8_t readStatus = Fm25clxx_HAL_SpiTransfer(0);

	Fm25clxx_HAL_ChipDeselect();
	return readStatus;
}

int Fm25clxx::test()
{
	uint16_t i;
	uint8_t data, readData;

	for (i = 0; i < MEMORY_SIZE; ++i) {
		readBlock(&data, sizeof(uint8_t), i);
		data = ~data;
		write(&data, sizeof(uint8_t), i);
		readBlock(&readData, sizeof(uint8_t), i);
		if (data != readData)	{
			return -1;
		}
	}
	return 0;
}

int Fm25clxx::calculateSize()
{
	uint8_t mark = 0xB0;
	uint8_t data = 0xA0;
	uint16_t i = 0;

	for (i = 0; i < 0xFFFF; ++i) {
		write(&data, sizeof(uint8_t), i);
	}

	write(&mark, sizeof(uint8_t), 0);

	for (i = 1; i < 0xFFFF; ++i) {
		readBlock(&data, sizeof(uint8_t), i);
		if (data == mark)	{
			return i;
		}
	}
	return -1;
}

void Fm25clxx::memset(uint8_t data)
{
	uint16_t i = 0;
	for (i = 0; i < MEMORY_SIZE; ++i) {
		write(&data, sizeof(uint8_t), i);
	}
}

void Fm25clxx::sendWriteEnableOpcode()
{
	Fm25clxx_HAL_ChipSelect();
	Fm25clxx_HAL_SpiTransfer(opcodeWREN);
	Fm25clxx_HAL_ChipDeselect();

	//60 нс необходимо перед следующим ChipEnable.
	asm("nop");
	asm("nop");
	asm("nop");
	asm("nop");
	asm("nop");
}

void Fm25clxx::write(const void* buf, size_t blockSize, uint16_t blockAdr)
{
	sendWriteEnableOpcode();

	Fm25clxx_HAL_ChipSelect();
	Fm25clxx_HAL_SpiTransfer(opcodeWRITE);
	Fm25clxx_HAL_SpiTransfer( (blockAdr >> 8) & 0xff );
	Fm25clxx_HAL_SpiTransfer( blockAdr & 0xff );

	size_t i;
	const uint8_t *ptrData = (const uint8_t*)buf;
	for (i = 0; i < blockSize; ++i) {

		Fm25clxx_HAL_SpiTransfer( ptrData[i] );
	}
	Fm25clxx_HAL_ChipDeselect();
}

Fm25clxx fm25clxx(0x2000);
