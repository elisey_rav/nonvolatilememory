#pragma once

#include <stdint.h>
#include <stddef.h>	//for size_t

class Fm25clxx
{
public:
	Fm25clxx(const unsigned int _size);

	int writeBlock( const void* buf, size_t blockSize, uint16_t blockAdr, bool check = true);
	void readBlock(void* buf, size_t blockSize, uint16_t blockAdr);
	void writeStatus(uint8_t newSatus);
	uint8_t readStatus();

	int test();
	int calculateSize();
	void memset(uint8_t data);

private:
	void sendWriteEnableOpcode();
	void write(const void* buf, size_t blockSize, uint16_t blockAdr);

	unsigned int MEMORY_SIZE;
};

extern Fm25clxx fm25clxx;
