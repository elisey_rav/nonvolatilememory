#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void Fm25clxx_HAL_Init();
uint8_t Fm25clxx_HAL_SpiTransfer(uint8_t data);
void Fm25clxx_HAL_ChipSelect();
void Fm25clxx_HAL_ChipDeselect();

#ifdef __cplusplus
}
#endif
