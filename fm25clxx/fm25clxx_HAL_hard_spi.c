#include "fm25clxx_HAL.h"

#include "spi.h"
#include "stm32f10x.h"

static spi_t *const ptrSpiDriver = &spi1;
static const uint32_t CS_RCC = RCC_APB2Periph_GPIOA;
static  GPIO_TypeDef* const CS_GPIO = GPIOA;
static const uint16_t CS_Pin = GPIO_Pin_4;

void Fm25clxx_HAL_Init()
{
	RCC_APB2PeriphClockCmd(CS_RCC, ENABLE);

	GPIO_InitTypeDef gpio;

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = CS_Pin;
	GPIO_Init(CS_GPIO, &gpio);

	Fm25clxx_HAL_ChipDeselect();

	SpiDriver_Init(ptrSpiDriver, spi_clock_mode_idle_low_1_edge, spi_first_bit_msb, spi_prescaler_4);
}

uint8_t Fm25clxx_HAL_SpiTransfer(uint8_t data)
{
	return SpiDriver_BlockingTransfer(ptrSpiDriver, data);
}

void Fm25clxx_HAL_ChipSelect()
{
	GPIO_WriteBit(CS_GPIO, CS_Pin, Bit_RESET);
}

void Fm25clxx_HAL_ChipDeselect()
{
	GPIO_WriteBit(CS_GPIO, CS_Pin, Bit_SET);
}
