#pragma once

#include "assert.h"

#define TEST_ASSERT(x)						assert(x)
#define TEST_ASSERT_DIFFERS(x,y)			assert(x != y)
#define TEST_ASSERT_EQUALS(x,y)				assert(x == y)

class DataManagerTest
{
public:
	DataManagerTest();
	void runTest();

private:
	void testMemory();
	void normalWriteRead();
	void corruptOneCell();
	void corruptTwoCells();
	void corruptTreeCells();
};
