#include "data_manager_terminal.h"

#include "microrl_func.h"
#include "fm25clxx.h"

#include <stdio.h>

static int prv_TerminalFunc_fram(int argc, const char * const * argv);
static void prv_printHead();
static void prv_printDataLine(uint32_t currentAddress, uint8_t *dataLine);

void DataManager_Terminal_Init()
{
	microrl_registerExecuteFunc(prv_TerminalFunc_fram, "fram", NULL);
}

const uint8_t coloumns =  0x10;

static int prv_TerminalFunc_fram(int argc, const char * const * argv)
{
	prv_printHead();

	uint32_t currentAddress = 0;
	uint8_t dataLine[coloumns];
	while (currentAddress < 0x2000)	{

		Fm25clxx_ReadBlock(dataLine, coloumns, currentAddress);
		prv_printDataLine(currentAddress, dataLine);
		currentAddress += coloumns;

	}
	return 0;
}

static void prv_printHead()
{
	puts("Fm25clxx dump\n");
	puts("Address   \t");
	int i;
	for (i = 0; i < coloumns; ++i) {
		printf("%02x ", i);
	}
	puts("\n");
	puts("---------------------------------------------------------------\n");
}

static void prv_printDataLine(uint32_t currentAddress, uint8_t *dataLine)
{
	printf("0x%08x\t", (unsigned int)currentAddress);

	int i;
	for (i = 0; i < coloumns; ++i) {
		printf("%02x ", dataLine[i]);
	}
	puts("\n");
}
