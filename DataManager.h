#pragma once

#include <stdint.h>
#include <stddef.h>

#define MAX_BLOCK_SIZE	(4)
#define MAJORITY_LEVEL	(5)

struct cell_t	{
	uint8_t block[MAX_BLOCK_SIZE];
	uint8_t blockSize;
	uint8_t equalityCounter;
};


class DataManager {
public:
	DataManager(size_t memoryOffset, size_t tableSize );
	int loadDataBlock(void *buf, size_t blockSize, uint16_t blockOffset);
	int saveDataBlock(const void *buf, size_t blockSize, uint16_t blockAdr);

private:
	void loadCellsFromExternalMemory(cell_t *ptrCells, uint16_t blockOffset);
	void makeXorBuffer(const uint8_t *source, uint8_t *destination, size_t size, uint8_t key);
	void makeXorCellData(cell_t *ptrCell, uint8_t key);
	bool cellsAreEqual(cell_t *ptrCells);
	bool compareCells(cell_t *cell1, cell_t *cell2);
	void calculateEqualityForCells(cell_t *ptrCells);
	int getMaxEquality(cell_t *ptrCells);
	int getCellIndexWithMaxEquality(cell_t *ptrCells, int maxEquality);
	void restoreMemory(const void *buf, size_t blockSize, uint16_t blockAdr);

	void readBlockFromMemory(void *buf, size_t blockSize, uint16_t blockAdr);
	int writeBlockToMemory(const void *buf, size_t blockSize, uint16_t blockAdr);


	size_t memoryOffset;
	size_t tableSize;
};
