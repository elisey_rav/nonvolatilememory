#include "DataManager.h"

#include <string.h>

#include "fm25clxx.h"
#include <assert.h>

#define debug_printf(X, p...)		((void)0)

uint8_t xorTable[7] = {0, 85, 101, 105, 133, 163, 173 };

DataManager::DataManager(size_t _memoryOffset, size_t _tableSize )
	:	memoryOffset(_memoryOffset),
	 	tableSize(_tableSize)
{
}

int DataManager::loadDataBlock(void *buf, size_t blockSize, uint16_t blockOffset)
{
	assert(blockSize <= MAX_BLOCK_SIZE);

	cell_t cells[MAJORITY_LEVEL];
	memset(cells, 0, sizeof(cell_t) * MAJORITY_LEVEL);
	int i;
	for (i = 0; i < MAJORITY_LEVEL; ++i) {
		cells[i].blockSize = blockSize;
	}
	loadCellsFromExternalMemory(cells, blockOffset);
	if (cellsAreEqual(cells) == true)	{
		//applyData
		memcpy(buf, &(cells[0]), blockSize );
		return 0;
	}

	calculateEqualityForCells(cells);
	int maxEquality;
	maxEquality = getMaxEquality(cells);

	/*maxMatchCount максимальное количество совпадений одной ячейки с другими. Если три ячейки имеют одинаковое
	 * значение, то maxEquality = 2. Для успешной загрузки данных, число одинаковых ячеек должно быть больше
	 * половины ((MAJORITY_LEVEL / 2) + 1). Учитывая что maxEquality - на единицу меньше одинаковых ячеек, то
	 * для успешной загрузки требуется не менее (MAJORITY_LEVEL / 2) + 1 - 1)
	*/
	if (maxEquality < (MAJORITY_LEVEL / 2) + 1 - 1)	{
		debug_printf("Can not restore data.\n");

		return (-1);
	}

	int index = getCellIndexWithMaxEquality(cells, maxEquality);

	restoreMemory(cells[index].block, blockSize, blockOffset);

	//applyData
	memcpy(buf, &(cells[index]), blockSize );
	debug_printf("Table was restored\n");
	return (0);
}

int DataManager::saveDataBlock(const void *buf, size_t blockSize, uint16_t blockAdr)
{
	assert(blockSize <= MAX_BLOCK_SIZE);

	int i;
	for (i = 0; i < MAJORITY_LEVEL; ++i) {
		uint16_t offsetWriteTo = blockAdr + (i * tableSize);

		uint8_t xoredBuffer[MAX_BLOCK_SIZE];
		makeXorBuffer((uint8_t*)buf, xoredBuffer, blockSize, xorTable[i]);

		int result = writeBlockToMemory(xoredBuffer, blockSize, offsetWriteTo);

		if (result != 0)	{
			return result;
		}
	}
	return 0;
}

void DataManager::loadCellsFromExternalMemory(cell_t *ptrCells, uint16_t blockOffset)
{
	int i;
	for (i = 0; i < MAJORITY_LEVEL; ++i) {
		uint16_t offsetReadFrom = blockOffset + (i * tableSize);
		readBlockFromMemory(ptrCells[i].block, ptrCells[i].blockSize, offsetReadFrom);
		makeXorCellData(&(ptrCells[i]), xorTable[i]);
	}
}

void DataManager::makeXorBuffer(const uint8_t *source, uint8_t *destination, size_t size, uint8_t key)
{
	size_t i;

	for (i = 0; i < size; ++i) {
		destination[i] = source[i] ^ key;
	}
}

void DataManager::makeXorCellData(cell_t *ptrCell, uint8_t key)
{
	makeXorBuffer(ptrCell->block, ptrCell->block, ptrCell->blockSize, key);
}

bool DataManager::cellsAreEqual(cell_t *ptrCells)
{
	int i;
	for (i = 1; i < MAJORITY_LEVEL; ++i) {
		if (compareCells(&(ptrCells[0]), &(ptrCells[i])) == false)	{
			return false;
		}
	}
	return true;
}

bool DataManager::compareCells(cell_t *cell1, cell_t *cell2)
{
	return (memcmp ( cell1->block, cell2->block, cell1->blockSize ) == 0);
}

void DataManager::calculateEqualityForCells(cell_t *ptrCells)
{
	int currentCellIndex;
	for (currentCellIndex = 0; currentCellIndex < MAJORITY_LEVEL; ++currentCellIndex) {
		int cellIndexCompareWith;
		for (cellIndexCompareWith = currentCellIndex + 1; cellIndexCompareWith < MAJORITY_LEVEL; ++cellIndexCompareWith) {
			if ( compareCells(&ptrCells[currentCellIndex], &ptrCells[cellIndexCompareWith]) )	{
				ptrCells[currentCellIndex].equalityCounter++;
				ptrCells[cellIndexCompareWith].equalityCounter++;
			}
		}
	}
}

int DataManager::getMaxEquality(cell_t *ptrCells)
{
	int maxMatchCount = 0;

	int i;
	for (i = 0; i < MAJORITY_LEVEL; ++i) {
		if (ptrCells[i].equalityCounter > maxMatchCount)	{
			maxMatchCount = ptrCells[i].equalityCounter;
		}
	}
	return maxMatchCount;
}

int DataManager::getCellIndexWithMaxEquality(cell_t *ptrCells, int maxEquality)
{
	int i;
	for (i = 0; i < MAJORITY_LEVEL; ++i) {
		if (ptrCells[i].equalityCounter == maxEquality)	{
			return i;
		}
	}
	return (-1);
}

void DataManager::restoreMemory(const void *buf, size_t blockSize, uint16_t blockAdr)
{
	saveDataBlock(buf, blockSize, blockAdr);
}

void DataManager::readBlockFromMemory(void *buf, size_t blockSize, uint16_t blockAdr)
{
	blockAdr += memoryOffset;
	fm25clxx.readBlock(buf, blockSize, blockAdr);
}

int DataManager::writeBlockToMemory(const void *buf, size_t blockSize, uint16_t blockAdr)
{
	blockAdr += memoryOffset;
	return fm25clxx.writeBlock(buf, blockSize, blockAdr, true);
}
