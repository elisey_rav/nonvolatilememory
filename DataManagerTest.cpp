#include "DataManagerTest.h"

#include "DataManager.h"
#include "NonVolatileVariableAllocator.h"
#include "fm25clxx.h"

DataManagerTest::DataManagerTest()
{

}


void DataManagerTest::runTest()
{
	testMemory();
	normalWriteRead();
	corruptOneCell();
	corruptTwoCells();
	corruptTreeCells();
}

void DataManagerTest::testMemory()
{
	fm25clxx.memset(0xff);
	/*int result = 0;
	result = fm25clxx.test();
	TEST_ASSERT(result == 0);*/
}

void DataManagerTest::normalWriteRead()
{
	uint32_t data = 0xFA439801;

	int result = dataManager.saveDataBlock(&data, sizeof(data), 4);
	TEST_ASSERT(result == 0);

	uint32_t readData = 0;

	result = dataManager.loadDataBlock(&readData, sizeof(data), 4);
	TEST_ASSERT_EQUALS(result, 0);
	TEST_ASSERT_EQUALS(readData, 0xFA439801);
}

void DataManagerTest::corruptOneCell()
{
	uint32_t data = 0x01010101;

	int result = fm25clxx.writeBlock(&data, sizeof(data), 4);
	TEST_ASSERT(result == 0);

	uint32_t readData;
	result = dataManager.loadDataBlock(&readData, sizeof(data), 4);
	TEST_ASSERT_EQUALS(result, 0);
	TEST_ASSERT_EQUALS(readData, 0xFA439801);

	fm25clxx.readBlock(&data, sizeof(data), 4);
	TEST_ASSERT_EQUALS(data, 0xFA439801);
}

void DataManagerTest::corruptTwoCells()
{
	uint32_t data = 0x01010101;

	int result = fm25clxx.writeBlock(&data, sizeof(data), 4);
	TEST_ASSERT(result == 0);

	data = 0;
	result = fm25clxx.writeBlock(&data, sizeof(data), 4 + sizeof(framData_t));
	TEST_ASSERT(result == 0);



	uint32_t readData;
	result = dataManager.loadDataBlock(&readData, sizeof(data), 4);
	TEST_ASSERT_EQUALS(result, 0);
	TEST_ASSERT_EQUALS(readData, 0xFA439801);
}

void DataManagerTest::corruptTreeCells()
{
	uint32_t data = 0x01010101;

	int result = fm25clxx.writeBlock(&data, sizeof(data), 4);
	TEST_ASSERT(result == 0);

	data = 0;
	result = fm25clxx.writeBlock(&data, sizeof(data), 4 + sizeof(framData_t));
	TEST_ASSERT(result == 0);

	data = 43;
	result = fm25clxx.writeBlock(&data, sizeof(data), 4 + sizeof(framData_t) * 2);
	TEST_ASSERT(result == 0);

	uint32_t readData;
	result = dataManager.loadDataBlock(&readData, sizeof(data), 4);
	TEST_ASSERT_EQUALS(result, -1);
}
